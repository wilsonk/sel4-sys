# sel4-sys

[![Crates.io](https://img.shields.io/crates/v/sel4-sys.svg?style=flat-square)](https://crates.io/crates/sel4-sys)

[Documentation](https://doc.robigalia.org/sel4_sys)

A Rust interface to the [seL4 kernel](https://sel4.systems). Raw syscall
bindings, kernel API, and data structure declarations.  Provides the same
interface that libsel4 does, with a few C-isms reduced.

## Status

Complete, though largely untested.
